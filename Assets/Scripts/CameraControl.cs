﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public Transform objective;
    public float Rdist = 15;
    public float Theta = 45; 
    public float Phi = 45;
    public float minZoom;
    public float maxZoom;


    LookCam lookObj;
    bool view2D = false; 

    // Start is called before the first frame update
    void Start()
    {
        lookObj = transform.GetChild(0).transform.GetComponent<LookCam>();
    }

    void FixedUpdate()
    {
        //setting what objective to  look at from lookCam Script;
        lookObj.lookObj = objective;

        ///Polar Coordinates
        float x = Rdist * Mathf.Sin(Theta * Mathf.PI / 180) * Mathf.Cos(Phi * Mathf.PI / 180);
        float z = Rdist * Mathf.Sin(Theta * Mathf.PI / 180) * Mathf.Sin(Phi * Mathf.PI / 180);
        float y = Rdist * Mathf.Cos(Theta * Mathf.PI / 180);

        Vector3 pos = objective.position + new Vector3(x, y, z);

        transform.position = Vector3.Lerp(transform.position, pos, 0.05f);
        transform.rotation = Quaternion.Lerp(transform.rotation, lookObj.transform.rotation, 0.06f);




    }

    public void change_angle(float angle)
    {
        Phi += angle;
    }
    //change the distance of the camera --> zoom
    public void change_zoom(float zoom)
    {
        Rdist += zoom;

        if (Rdist < minZoom)
        {
            Rdist = minZoom;
        }
        if (Rdist > maxZoom)
        {
            Rdist = maxZoom;
        }

    }

    public void changePerspective()
    {
        view2D = !view2D;
        if(view2D)
        {
            Rdist = 40;
            Theta = 0;
            Phi = 0;
        }
        else
        {
            Rdist = 40;
            Theta = 45;
            Phi = -45;
        }
    }
}
