﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    public GameObject missile;
    public GameObject silo1, silo2;
    Game gameManager;

    Transform container;

    public int p = 0;
    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<Game>();
        container = GameObject.FindGameObjectWithTag("Missile").transform;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void missileLaunch(Transform target)
    {
        Debug.Log("Launch Missle");
        Transform origin;
        gameManager.click = 0;
        gameManager.shoot = true;

        if (p == 1)
        {
            origin = silo1.transform;
        }
        else
        {
            origin = silo2.transform;
        }

        GameObject a = Instantiate(missile, origin.position, Quaternion.Euler(0, 0, 0));
        a.transform.SetParent(container);

        Missile missileScript = a.GetComponent<Missile>();
        missileScript.target = target;
        missileScript.origin = origin;


    }
}
