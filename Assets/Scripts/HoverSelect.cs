﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoverSelect : MonoBehaviour
{
    Game gameManager;
    CameraControl camCtrl;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<Game>();
        camCtrl = GameObject.Find("Main Camera").GetComponent<CameraControl>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void onClick()
    {
        if(gameManager.player == -1 || gameManager.player == -2)
        {
            camCtrl.objective = transform;
        }
        gameManager.clickObject = gameObject;
        gameManager.click += 1;
    }

    public void hoverEnter()
    {
        gameManager.overObject = gameObject;
    }
    public void hoverExit()
    {
        gameManager.overObject = null;
    }
}
