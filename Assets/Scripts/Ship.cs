﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour
{
    Game gameManager;
    Canvas canvas;

    public int impactCount;
    public int life = 3;
    public bool destroyed = false;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<Game>();
        canvas = transform.GetComponent<Canvas>();

    }

    // Update is called once per frame
    void Update()
    {
        if (life<=impactCount)
        {
            destroyed = true;
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<Game>();
        gameManager.canPlace = false;
    }

    public void OnTriggerExit(Collider other)
    {
        gameManager.canPlace = true;
    }
    public void disableCanvas()
    {
        canvas.enabled = false;
    }
}
