﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Missile : MonoBehaviour
{
    public Transform origin;
    public Transform target;

    public float speed = 0.3f;
    public float yMax = 10;

    public GameObject impact;

    Game gameManager;
    Transform container;
    bool impactedShip = false;


    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<Game>();
        container = GameObject.FindGameObjectWithTag("Impacts").transform;

        StartCoroutine(MissleLaunch());
    }

    IEnumerator MissleLaunch()
    {
        Vector3 dir = target.position - origin.transform.position;

        float xMax = dir.magnitude;

        dir = dir / dir.magnitude;

        //Parabola equation
        float a = -2 * yMax / Mathf.Pow(xMax, 2);
        float b = -a * xMax;

        float t0 = Time.fixedTime;

        while ((transform.position - target.position).magnitude > 0.5f)
        {
            float t = Time.fixedTime - t0;
            float x = Mathf.Sqrt(Mathf.Pow((transform.position - origin.position).x, 2) + Mathf.Pow((transform.position - origin.position).z, 2));

            float vy = 2 * a * x + b;
            Vector3 vel = dir * speed + new Vector3(0, vy, 0);

            transform.position = transform.position + vel;

            transform.forward = vel;

            yield return new WaitForFixedUpdate();

        }

        transform.rotation = Quaternion.Euler(90, 0, 0);
        transform.GetComponent<Rigidbody>().useGravity = true;

        Invoke("ChangePlayer", 3);

    }

    public void ChangePlayer()
    {
        gameManager.changePlayer();   
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Water")
        {
            GameObject impactShow = Instantiate(impact, target.position, Quaternion.Euler(90, 0, 0)) as GameObject;

            
            transform.GetComponent<MeshRenderer>().enabled = false;
            Destroy(gameObject, 12);

            impactShow.transform.GetChild(0).GetComponent<Image>().enabled = true;
            impactShow.transform.SetParent(container);
        }

        else if (other.gameObject.tag == "ship")
        {
            if(impactedShip == false)
            {
                Ship shipImpact;
                shipImpact = other.GetComponent<Ship>();
                shipImpact.impactCount += 1;

                GameObject impactShow = Instantiate(impact, target.position, Quaternion.Euler(90, 0, 0)) as GameObject;

                impactShow.transform.GetChild(1).GetComponent<Image>().enabled = true;
                impactShow.transform.SetParent(container);

                impactedShip = true;

                transform.GetComponent<Rigidbody>().useGravity = false;
                transform.GetComponent<Rigidbody>().velocity = new Vector3();
            }
            else
            {

            }
           
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "ship")
        {
            transform.GetComponent<Rigidbody>().useGravity = false;
            transform.GetComponent<Rigidbody>().velocity = new Vector3();
        }
        
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
