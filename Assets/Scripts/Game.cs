﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    public GameObject[] boats;
    public int player = 1;

    public int selectedBoat = 0;
    public GameObject overObject, clickObject;
    public int click = 0;

    public bool canPlace = true;

    public Transform P1pos;
    public Transform P2pos;

    public GameObject[] player1_boats, player2_boats;
    public GameObject shooting_square;

    public bool shoot;

    public Transform c_p1, c_p2;

    CameraControl cameraCtrl;
    Shooting shootingScript;
    
    // Start is called before the first frame update
    void Start()
    {
        player1_boats = new GameObject[boats.Length];
        player2_boats = new GameObject[boats.Length];

        player1_boats[selectedBoat] = GameObject.Instantiate(boats[selectedBoat], new Vector3(20000, 20000, 20000), Quaternion.Euler(0, 0, 0)) as GameObject;
        player1_boats[selectedBoat].transform.SetParent(c_p1);

        cameraCtrl = Camera.main.GetComponent<CameraControl>();
        shootingScript = shooting_square.GetComponent<Shooting>();

        cameraCtrl.objective = P1pos;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void FixedUpdate()
    {
        //Start Placing Phase
        if (player == 1) //Player 1 Placing Boats 
        {
            if (overObject != null)
            {
                if (overObject.tag == "GridPlayer1")
                {
                    if (click == 0)//First Finding the origin placement of the boat.
                    {
                        player1_boats[selectedBoat].transform.position = overObject.transform.position;
                    }
                    else if (click == 1) //then the dirrection 
                    {
                        Vector3 dir = overObject.transform.position - player1_boats[selectedBoat].transform.position;
                        if (Mathf.Abs(dir.z / dir.x) > 1e5 || Mathf.Abs(dir.z / dir.x) < 0.1) //no diagonals 
                        {
                            player1_boats[selectedBoat].transform.forward = dir;
                        }
                    }
                    else if (click == 2)
                    {
                        if (canPlace)
                        {
                            player1_boats[selectedBoat].transform.GetChild(0).GetComponent<Ship>().disableCanvas();

                            selectedBoat += 1;

                            if (selectedBoat >= boats.Length)
                            {
                                player = 2;
                                selectedBoat = 0;

                                cameraCtrl.objective = P2pos;
                                cameraCtrl.Phi = 180 - 45;

                                disableBoats(player1_boats);

                                player2_boats[selectedBoat] = GameObject.Instantiate(boats[selectedBoat], new Vector3(20000, 20000, 20000), Quaternion.Euler(0, 0, 0)) as GameObject;
                                player2_boats[selectedBoat].transform.SetParent(c_p2);
                            }
                            else
                            {
                                player1_boats[selectedBoat] = GameObject.Instantiate(boats[selectedBoat], new Vector3(20000, 20000, 20000), Quaternion.Euler(0, 0, 0)) as GameObject;
                                player1_boats[selectedBoat].transform.SetParent(c_p1);
                            }
                            click = 0;
                        }
                        else
                        {
                            click = 0;
                        }
                    }

                }
            }
        }

        else if (player == 2)
        {
            if (overObject != null)
            {
                //only if selected a grid square of player 1
                if (overObject.tag == "GridPlayer2")
                {
                    if (click == 0)
                    {
                        player2_boats[selectedBoat].transform.position = overObject.transform.position;
                    }
                    else if (click == 1)
                    {
                        //we obtain the direction for rotation
                        Vector3 dir = overObject.transform.position - player2_boats[selectedBoat].transform.position;
                        // diagonals are not allowed
                        if (Mathf.Abs(dir.z / dir.x) > 1e5 || Mathf.Abs(dir.z / dir.x) < 0.1)
                        {
                            player2_boats[selectedBoat].transform.forward = dir;
                        }
                    }
                    else if (click == 2)
                    {
                        //only if it is possible to place the boat, create a new boat
                        if (canPlace)
                        {
                            player2_boats[selectedBoat].transform.GetChild(0).GetComponent<Ship>().disableCanvas();

                            //increase the boat number
                            selectedBoat += 1;

                            //when there is no more boats to place
                            if (selectedBoat >= boats.Length)
                            {
                                player = -1;

                                //go to player2 zone
                                cameraCtrl.objective = P1pos;
                                cameraCtrl.Phi = -45;

                                //disable boats
                                disableBoats(player2_boats);
                                //mess_player1.SetActive(true);

                            }
                            else
                            {
                                //instantiate boat and set parent
                                player2_boats[selectedBoat] = GameObject.Instantiate(boats[selectedBoat], new Vector3(20000, 20000, 20000), Quaternion.Euler(0, 0, 0)) as GameObject;
                                player2_boats[selectedBoat].transform.SetParent(c_p2);
                            }
                            click = 0;
                        }
                        else
                        {
                            click = 0;
                        }
                    }
                }
            }
        }
        //End Placing Phase

        //Battle Phase
        else if (player == -1 )
        {
            //Show menu for Player 1 UI

            enableBoats(player1_boats);
            disableBoats(player2_boats);

            if(clickObject != null)
            {
                if(click > 0 && clickObject.tag == "GridPlayer2" && !shoot)
                {
                    shooting_square.gameObject.SetActive(true);
                    shooting_square.transform.position = clickObject.transform.position;
                    shootingScript.p = 1;
                }
                else
                {
                    shooting_square.SetActive(false);
                }
            }

            if(checkGameOver(player1_boats, player2_boats))
            {
                
            }
            
        }

        else if (player == -2)
        {
            //Show menu for Player 1 UI

            enableBoats(player2_boats);
            disableBoats(player1_boats);

            if (clickObject != null)
            {
                if (click > 0 && clickObject.tag == "GridPlayer1" && !shoot)
                {
                    shooting_square.gameObject.SetActive(true);
                    shooting_square.transform.position = clickObject.transform.position;
                    shootingScript.p = 2;
                }
                else
                {
                    shooting_square.SetActive(false);
                }
            }

            if (checkGameOver(player1_boats, player2_boats))
            {
                //GameOver UI
            }

        }
    }


    public void changePlayer()
    {
        if (player == -1)
        {
            //mess_player2.SetActive(true);
            player = -2;
            shoot = false;
            cameraCtrl.objective = P2pos;
            cameraCtrl.Phi = 180 - 45;
        }
        else if (player == -2)
        {
           // mess_player1.SetActive(true);
            player = -1;
            shoot = false;
            cameraCtrl.objective = P1pos;
            cameraCtrl.Phi = -45;
        }
    }

    public void disableBoats(GameObject[] go)
    {
        for (int ii = 0; ii < go.Length; ii++)
        {
            //boat script in order to check if it is destroyed or not
            Ship boatS;
            boatS = go[ii].transform.GetChild(0).GetComponent<Ship>();
            //if it is destroyed, show up the boat
            if (boatS.destroyed == false)
            {
                //we need to dissable all the meshrenderers of the game object
                MeshRenderer[] renderers;
                renderers = go[ii].GetComponentsInChildren<MeshRenderer>();

                foreach (MeshRenderer renderer in renderers)
                {
                    renderer.enabled = false;
                }
            }
        }
    }

    public void enableBoats(GameObject[] go)
    {
        for (int ii = 0; ii < go.Length; ii++)
        {
            //we need to enable all the meshrenderers of the game object
            MeshRenderer[] renderers;
            renderers = go[ii].GetComponentsInChildren<MeshRenderer>();
            foreach (MeshRenderer renderer in renderers)
            {
                renderer.enabled = true;
            }
        }
    }

    bool checkGameOver(GameObject[] g1, GameObject[] g2)
    {
        //check if the player 1 or player 2 has been destroyed
        bool end1 = true;
        bool end2 = true;

        for (int ii = 0; ii < g1.Length; ii++)
        {
            end1 = end1 & g1[ii].transform.GetChild(0).GetComponent<Ship>().destroyed;
        }

        for (int ii = 0; ii < g2.Length; ii++)
        {
            end2 = end2 & g2[ii].transform.GetChild(0).GetComponent<Ship>().destroyed;
        }

        return (end1 || end2);
    }

}
